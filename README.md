# Introduction

Site web à réalisé à l'image d'un site événementiel..

# Auteur

Allouch Yanis et Feng-Ly Txu

# Ressources utilisées

* Langage : 
    * PHP7+
    * JS
    * HTML5
    * CSS3
* Framework/Librairies/Addons : 
    * Jquery (v3.4.1)
    * Jquery UI (v1.12.1)
    * OpenLayers (v5?) + OpenStreetMap (Cartes)
    * Bootstrap (v4.3.1)
    * W3.CSS (v4)
    * FontAwesome (icon)
* Source : 
    * w3schools.com
    * mapado.com 
    * startbootstrap.com
    * openclassroom.com
    * developpez.com
    * mozilla.com
    * wikipedia
    * ...

# ReadMe

* email (backup) :jibigol@mailseo.net
* email : lliego_international@mail.com
* mdp : Lliego@123
