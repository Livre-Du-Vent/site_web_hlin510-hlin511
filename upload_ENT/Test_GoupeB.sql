/*
Fichier : Test_GroupeB.sql
Auteurs : 
ALLOUCH YANIS 21708237
LY TXU-FENG 21606179
Nom du groupe : B
*/

/* ---------------------------------------------Quelques requêtes------------------------------------------------------------------------*/

/* Selectionne info sur un evenement  */
SELECT titre, evenements.description, effectif_max, date_evenement , heure_evenement, num_rue, nom_voie, cp, longitude, latitude
FROM evenements,adresses
WHERE evenements.ida = adresses.ida
AND evenements.ide = 1;

/* selectionnne les commentaire sur un evenement */
SELECT note, commentaire, nom, prenom
FROM commente, users, evenements
WHERE commente.IDUSER = users.IDUSER
AND commente.IDE = evenements.IDE
AND evenements.IDE=1;

/* selectionne le ou les theme d'un evenement */
SELECT mot 
FROM evenements, compose
WHERE evenements.IDE = compose.IDE
AND evenements.ide = 1;

/* ---------------------------------------------Test des triggers----------------------------------------------------------------------*/

/* Avant une insertion dans COMMENTE*/
/* Teste si l'utilisateur essaye de commenter un événement qui n'est pas encore passée */
INSERT INTO `commente`(`IDE`, `IDUSER`, `NOTE`, `COMMENTAIRE`) VALUES (9, 8, 5 , 'Je sens que ça va être génial') ;
/* Teste si l'utilisateur commente alors qu'il ne participe pas à l'événement. */
INSERT INTO `commente`(`IDE`, `IDUSER`, `NOTE`, `COMMENTAIRE`) VALUES (10,8, 1 , 'Cet événement est nul.') ;

/* Avant une insertion dans PARTICIPE*/
/* lorsqu'un utilisateur souhaite participer à un événement alors qu'il participe déjà à un autre événement à la même date et à la même heure.*/
INSERT INTO PARTICIPE VALUES (11, 9);
/* lorsqu'un utilisateur souhaite participer à un événement alors que le nombre maximum de participant est déjà dépassé */
INSERT INTO PARTICIPE VALUES (1, 9);

/* Avant une insertion dans users*/
/* l'utilisateur s'inscrit avec un mail qui existe déjà dans la base donnée */
INSERT INTO USERS VALUES (12, 4, "Nouvel", "Utilisateur", "foobar1@gmail.com", "azertyuiop", '2019-12-19');


/* ---------------------------------------------Tests fonctions-------------------------------------------------------------------*/

/* Renvoie faux car il n'existe pas d'utilisateur identifié par 99 ni d'évenement a ce jour */
SELECT PARTICIPE_A(99,99);
  
/* Renvoie vrai car il existe un utilisateur identifié par 1 et participe a l'évenement 1 */
SELECT PARTICIPE_A(1,1);

/* Renvoie 2 car il existe 2 participant l'évenement 1 */
SELECT NOMBRE_PARTICIPANT(1);

/* Renvoie 0 car il n'existe pas de participant l'évenement 99 (qui n'existe pas) */
SELECT NOMBRE_PARTICIPANT(99);

/* ---------------------------------------------Tests procédure-------------------------------------------------------------------------*/

/* Retourne les 10 premiers événements */
CALL TOP10_EVENEMENT();

/* Déclenche la SQLSTATE 01H01 et le message d'erreur 'l utilisateur n a pas participer a l événement.' */
CALL GESTION_ERREUR(1);

/* Déclenche la SQLSTATE 01H07 et le message d'erreur 'Erreur non prise en charge, vérifiez la documentation' */
CALL GESTION_ERREUR(99);

/* Retourne les événements actifs */
CALL EVENEMENTS_ACTIF_ARCHIVER(1);

/* Retourne les événements inactif/archiver */
CALL EVENEMENTS_ACTIF_ARCHIVER(0);

/* Retourne les informations de l'événement 1 */
CALL INFORMATION_EVENEMENT(1);

/* Retourne un resultat vide car l'événement 99 n'existe pas */
CALL INFORMATION_EVENEMENT(1);