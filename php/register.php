<!DOCTYPE html>
<html>
<head>
	<title>Lliego International</title>
	<link rel="icon" href="/img/logo1v2.ico" />
	<meta http-equiv="Content-Type" content="text/php; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<link rel="stylesheet" href="/fontawesome-free-5.11.2-web/css/all.css">
	<script src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
</head>
<body class="w3-light-grey">
	<?php error_reporting( E_ALL );  include 'navbar.php';?>

	<div class="w3-container w3-margin-top" id="register">
		<form method="POST">
			<input class="w3-input w3-half w3-border" type="email" placeholder="Email" required name="email" autofocus autocomplete="on" maxlength="100">			
			<input class="w3-input w3-half w3-border" type="password" placeholder="Password" required name="password" autocomplete="on" maxlength="15">
			<input class="w3-input w3-half w3-border" type="text" name="nom" placeholder="Nom" required autocomplete="on" maxlength="50">
			<input class="w3-input w3-half w3-border" type="text" name="prenom" placeholder="Prenom" required autocomplete="on" maxlength="50">
			<input class="w3-input w3-half w3-border" type="number" name="numeroDeRue" placeholder="Numéro de rue : 00" required autocomplete="on" step="1" maxlength="65">
			<input class="w3-input w3-half w3-border" type="text" name="nomDeRue" placeholder="Nom de Rue" required autocomplete="on" maxlength="500">
			<input class="w3-input w3-border" type="number" name="codePostal" placeholder="Code Postal : 00000" required autocomplete="on" step="1" maxlength="10">
			<button class="w3-button w3-black w3-padding-large w3-border " type="submit" name="submit">Submit</button>
		</form>
	</div>
	
	<?php
	if( empty($_POST['submit']) ){
		if ( empty($_POST['email']) or empty($_POST['password']) or empty($_POST['nom']) or empty($_POST['prenom']) or empty($_POST['numeroDeRue']) or empty($_POST['nomDeRue']) or empty($_POST['codePostal']) ) {
			$message = "it\'s empty";
		//echo "<script type='text/javascript'>alert('$message');</script>";
		}
		else{
			$dbname='bdlliegointernational';
			$host='localhost';
			$strConnection = 'mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8';
			$arrExtraParam = array(1002 => 'SET NAMES utf8');
			$user = 'root';
			$pwd = '';

			$nbIdAdresses;
			$nbIdUsers;
			try{
				$bdd = new PDO($strConnection, $user, $pwd, $arrExtraParam);
				$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				$queryCountAdresses = "SELECT COUNT(*) FROM ADRESSES;";
				
				$rep = $bdd->prepare($queryCountAdresses);
				$rep->execute();
				$nbIdAdresses = 0 + $rep->fetch()[0] + 1;
				
				$queryAdresse = "INSERT INTO ADRESSES VALUES({$nbIdAdresses}, {$_POST['numeroDeRue']}, '{$_POST['nomDeRue']}', {$_POST['codePostal']}, 0, 0);";
				$rep = $bdd->prepare($queryAdresse);
				$rep->execute();

				$rep = $bdd->prepare("SELECT users.mail FROM users WHERE mail = ':email';");
				$rep->execute(array("email" => $_POST['email']));
				$rep = $rep->fetch()[0];
				if (strcasecmp($_POST['email'],$rep) != 0) {
					$queryCountUsers = "SELECT COUNT(*) FROM USERS;";
					$rep = $bdd->prepare($queryCountUsers);
					$rep->execute();
					$nbIdUsers = 0 + $rep->fetch()[0] + 1;

					$queryUser = "INSERT INTO USERS VALUES({$nbIdUsers}, {$nbIdAdresses}, '{$_POST['nom']}', '{$_POST['prenom']}', '{$_POST['email']}', '{$_POST['password']}', '" . date('Y-m-d') . "');";
					$rep = $bdd->prepare($queryUser);
					$rep->execute();
					
				}
				else {
					$queryAdresse = "DELETE FROM ADRESSES WHERE IDA = {$nbIdAdresses};";
					$rep = $bdd->prepare($queryAdresse);
					$rep->execute();
				}
			}
			catch (PDOException $e) {
				$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				$queryAdresse = "DELETE FROM ADRESSES WHERE IDA = {$nbIdAdresses};";
				$rep = $bdd->prepare($queryAdresse);
				$rep->execute();
				$queryUser = "DELETE FROM USERS WHERE IDUSER = {$nbIdUsers};";
				$rep = $bdd->prepare($queryUser);
				$rep->execute();
				die($msg);
			}
			catch (Exception $e){
				$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				die($msg);
			}
			?>
			<script type="text/javascript">
				window.location.href = '/index.php';
			</script>
			<?php
		}
	}

	?>
	<?php include 'about.php';?>
	<?php include 'footer.php';?>
</body>
</html>