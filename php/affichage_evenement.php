<?php
$i=0;
while (($row = $rep->fetch())) {
	if($i%3 == 0) {
		echo "<div class='w3-row-padding w3-padding-16'>";
	}

	echo "<div class='w3-third w3-margin-bottom w3-hover-sepia' id='event-{$row['IDE']}'><form method='POST' action='/php/evenement.php'><img src='/img/imgevent-{$row['IDE']}-{$row['DATE_EVENEMENT']}.jpg' alt='{$row['TITRE']}' style='height:260px;width:100%' class='w3-image'><div class='w3-container w3-white'>";
	echo "#IDE{$row['IDE']}";
	echo "<h3>{$row['TITRE']}</h3>";
	echo "<h6 class='w3-red w3-border w3-border'>Tarif : {$row['TARIF']} €</h6>";
	echo "<p>Effectif maximum de {$row['EFFECTIF_MAX']}</p>";
	echo ("<p>" . substr($row['DESCRIPTION'], 0, 20) . " ...</p>");
	echo ("<p>Localisation : " . $row['CP'] . "</p>");
	echo "<p class='w3-large'></p><button class='w3-button w3-block w3-black w3-margin-bottom' name='affichage'>S'enregistrer</button></div><input id='event{$row['IDE']}Info' name='eventId' type='hidden' value='{$row['IDE']}'></form></div>";
	$i++;
	
	if($i%3 == 0){
		echo "</div>";
	}
}
?>