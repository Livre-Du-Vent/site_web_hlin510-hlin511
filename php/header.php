<!-- Header -->
<header class="w3-display-container w3-content" style="max-width:1500px;min-width:1000px;min-height:400px">
	<div class="w3-padding w3-col 16 m12">
		<div class="w3-container w3-red" style="width:100%; height:75%">
			<h2>Carte des événements disponibles</h2>
		</div>
		<div id="map" class="map"  style="width:100%; height:75%"></div>
		<div id="accordion"></div>
		<img id="markerProtoGreen" class="marker" src="/img/markergreen.png" width="50" height="50"/>
		<img id="markerProtoRed" class="marker" src="/img/markerred.png" width="50" height="50"/>
		<div id="popupProto" class="popup"></div>
	</div>
</header>
