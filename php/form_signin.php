<div class="w3-container" id="signin">
		<form method="POST">
			<label>Email</label><input class="w3-input w3-animate-input" type="email" placeholder="Email" required name="email" autofocus>			
			<label>Password</label><input class="w3-input   w3-animate-input" type="password" placeholder="Password" required name="password">

			<button class="w3-button w3-black w3-padding-large w3-border" type="submit" name="signin">Sign-in</button>
		</form>
		<form method="POST" action="/php/register.php">
			<button class="w3-button w3-black w3-padding-large w3-border" type="submit" name="register" target="/php/register.php">Register</button>
		</form>
</div>