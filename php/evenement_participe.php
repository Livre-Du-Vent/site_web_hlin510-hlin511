<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lliego International</title>
	<link rel="icon" href="/img/logo1v2.ico" />
	<meta http-equiv="Content-Type" content="text/php; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<link rel="stylesheet" href="/fontawesome-free-5.11.2-web/css/all.css">
	<script src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
</head>
<body class="w3-light-grey">
	<?php error_reporting( E_ALL );?>  
	<?php include 'info_bdd.php';?>
	<?php include 'navbar.php';?>





	<?php

	try{

		$bdd=new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8',$username,$password);
		if(isset($_POST['submit'])){
			echo $_POST['supp']."    ".$_SESSION['id'];
			$requete = $bdd->prepare("DELETE FROM participe WHERE ide={$_POST['supp']} AND idusers={$_SESSION['id']}");
			$requete->execute();
		}

		$requete = $bdd->prepare("SELECT evenements.ide, titre, date_evenement FROM evenements, participe WHERE evenements.ide=participe.ide AND iduser = :var_id ;");
		$requete->execute(array(":var_id" => $_SESSION['id']));

	// afficher tous les evenement auquels l'utilisateur participe + bouton ne plus participer
		echo "<ul class='w3-ul'>";
		while($data_user = $requete->fetch()){
			echo "<li id='li{$data_user['ide']}'>";
			echo "<h3>".$data_user['titre']."</h3>";
			echo $data_user['date_evenement'];
			echo "<form method='POST' name='".$data_user['ide']."'>
			<input type='hidden' name='supp' value='{$data_user['ide']}'></input>
			<button class='w3-button w3-light-grey' name='submit'".
		// onclick='supprimerBlock({$data_user['ide']})'.
			">Ne plus participer</button>";
			echo "</form></li>";
		}
		echo "</ul>";

	// echo "<script type='text/javascript'>function supprimerBlock(id) {let liste = document.getElementById('li' + id);liste.remove();}</script>";
	}catch (PDOException $e) {
		$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
		die($msg);
	}
	catch (Exception $e){
		$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
		die($msg);
	}

	?>

	<?php include 'about.php';?>
	<?php include 'footer.php';?>
</body>
</html>