<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lliego International</title>
	<link rel="icon" href="/img/logo1v2.ico" />
	<meta http-equiv="Content-Type" content="text/php; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<link rel="stylesheet" href="/fontawesome-free-5.11.2-web/css/all.css">
	<script src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
</head>
<body class="w3-light-grey">
	<?php include 'info_bdd.php';?>
	<?php include 'navbar.php';?>
	<?php
	
	$dbname='bdlliegointernational';
	$host='localhost';
	$strConnection = 'mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8';
	$arrExtraParam = array(1002 => 'SET NAMES utf8');
	$user = 'root';
	$pwd = '';
	$foo_arr;

	$ideCourant = 0;
	if (isset($_POST['affichage'])){
		if(isset($_POST['eventId'])){
			try{
				$bdd = new PDO($strConnection, $user, $pwd, $arrExtraParam);
				$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

				$queryEvents = "CALL INFORMATION_EVENEMENT({$_POST['eventId']});";
				$rep = $bdd->prepare($queryEvents);
				$rep->execute();
				$row = $rep->fetchAll();
				$rep->closeCursor();

				$queryNombreParticipe = "SELECT NOMBRE_PARTICIPANT({$_POST['eventId']});";
				$rep2 = $bdd->prepare($queryNombreParticipe);
				$rep2->execute();
				$row2 = $rep2->fetchAll();
				$rep2->closeCursor();

				$queryContributeur = "SELECT USERS.NOM, USERS.PRENOM FROM EVENEMENTS, CONTRIBUTEURS, USERS WHERE EVENEMENTS.IDCONTRIB = CONTRIBUTEURS.IDCONTRIB AND EVENEMENTS.IDE = {$_POST['eventId']}; AND CONTRIBUTEURS.IDCONTRIB = USERS.IDUSER;";
				$rep3 = $bdd->prepare($queryContributeur);
				$rep3->execute();
				$row3 = $rep3->fetchAll();
				$rep3->closeCursor();

				echo "<div class='w3-content'>";

				$ideCourant = $row['0']['IDE'];

				echo "<div class='w3-twohalf w3-margin-bottom' id='event-{$ideCourant}'><form method='POST' action='/php/evenement.php' id='form{$ideCourant}'><img src='/img/imgevent-{$ideCourant}-{$row['0']['DATE_EVENEMENT']}.jpg' alt='{$row['0']['TITRE']}' style='width:100%' class='w3-image'><div class='w3-container w3-white'>";
				echo "#IDE{$ideCourant}";
				echo "<h3>{$row['0']['TITRE']}</h3>";
				echo "<h6 class='w3-red w3-border w3-border'>Tarif : {$row['0']['TARIF']} €</h6>";

				echo "<p>Effectif courant de <b>{$row2['0']['0']} </b>/ {$row['0']['EFFECTIF_MAX']}</p>";

				echo ("<p>" . $row['0']['DESCRIPTION'] . "</p>");
				echo ("<p>Localisation : " . $row['0']['CP'] . "</p>");

				echo ("<p>Proposé par : " . $row3['0']['NOM'] . ' ' . $row3['0']['PRENOM'] . "</p>");

				echo "<p class='w3-large'></p><button class='w3-button w3-block w3-black w3-margin-bottom' name='participation' id='button{$ideCourant}'>Participer</button></div><input id='event{$ideCourant}Info' name='eventId' type='hidden' value='{$ideCourant}'></form></div>";
				if(empty($_SESSION)){
					echo "<script type='text/javascript'>$().ready(function(){document.getElementById('form{$ideCourant}').setAttribute('action', '/php/signin.php');});</script>";
				}

				/*
				if($requete){
					$data_user = $requete->fetch();
					if(count($data_user)>1){
						if(strcmp($_POST['password'], $data_user['MDP'])== 0){
							$_SESSION['id'] = $data_user['IDUSER'];
							$_SESSION['nom'] = $data_user['NOM'];
							$_SESSION['prenom'] = $data_user['PRENOM'];
							$_SESSION['email'] = $data_user['MAIL'];
							$_SESSION['type'] = 'user';

																	//vérification si l'utilisateur est un contributeur
							$requete = $bdd->prepare("SELECT * from contributeurs WHERE idcontrib = ?;");
							$requete->execute(array($data_user['IDUSER']));
							$data_contrib = $requete->fetch();
							if(count($data_contrib) > 1){
								$_SESSION['type'] = "contrib";
							}

							$requete = $bdd->prepare("SELECT * from administrateurs WHERE idadmin = ?;");
							$requete->execute(array($data_user['IDUSER']));
							$data_admin = $requete->fetch();
							if(count($data_admin) > 1){
								$_SESSION['type'] = "admin";
							}
									header("Location: /index.php"); // Retour page d'accueil
								}
								else{
									 // mot de passe incorrect
									include 'form_signin.php';
									echo "<p> votre mot de passe est incorrect.</p>";
								}
							}
							else{
															 // email incorrect
								include 'form_signin.php';
								echo "<p>votre mail est incorrect<p>";
							}
						}
						else{ 
						// problème de connexion à la BDD
							include 'form_signin.php';
							echo "<p>connexion à base donnée impossible<p>";
						}
					}
				}*/
				echo "</div>";
			}
			catch (PDOException $e) {
				$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				die($msg);
			}
			catch (Exception $e){
				$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				die($msg);
			}
		}
	}
	else{
		try{
			$bdd = new PDO($strConnection, $user, $pwd, $arrExtraParam);
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			$queryParticipe = "SELECT PARTICIPE_A({$ideCourant}, {$_SESSION['id']});";
			$rep = $bdd->prepare($queryParticipe);
			$rep->execute();
			$rep = $rep->fetchAll()[0][0];
			if($rep){
				echo "<script type='text/javascript'>$().ready(function(){document.getElementById('button{$ideCourant}').value = 'Vous participez déjà';});</script>";
			}
		}
		catch (PDOException $e) {
			$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
			die($msg);
		}
		catch (Exception $e){
			$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
			die($msg);
		}
		if(isset($_POST['participation']) && !($rep)){
			try{
				$bdd = new PDO($strConnection, $user, $pwd, $arrExtraParam);
				$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
				$queryParticipation = "INSERT INTO PARTICIPE VALUES ({$ideCourant}, {$_SESSION['id']});";
				$rep = $bdd->prepare($queryParticipation);
				$rep->execute();
			}
			catch (PDOException $e) {
				$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage() . ' || ' ;
				die($msg);
			}
			catch (Exception $e){
				$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				die($msg);
			}
		}
		echo "<div class=' w3-center w3-content w3-black w3-border w3-padding'>Oups, something went wrong ! You should have select an event before coming here :-)</div>";
	}
	?>
	<?php include 'about.php';?>
	<?php include 'footer.php';?>
</body>
</html>