<!-- Page content -->
<div class="w3-content" style="max-width:1532px;">
	<?php include 'bar_search.php'; ?>
	<?php
	$dbname='bdlliegointernational';
	$host='localhost';
	$strConnection = 'mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8';
	$arrExtraParam = array(1002 => 'SET NAMES utf8');
	$user = 'root';
	$pwd = '';

	$nbIdAdresses;
	$nbIdUsers;
	try{
		$bdd = new PDO($strConnection, $user, $pwd, $arrExtraParam);
		$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$queryEvents = "CALL TOP10_EVENEMENT();";

		$rep = $bdd->prepare($queryEvents);
		$rep->execute();
		include 'affichage_evenement.php';
	}
	catch (PDOException $e) {
		$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
		die($msg);
	}
	catch (Exception $e){
		$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
		die($msg);
	}
	?>
</div>

	<!-- Exemple d'une liste d'événements 
	<div class="w3-row-padding w3-padding-16">
		<div class="w3-third w3-margin-bottom">
			<img src="/img/upcoming-event.jpg" alt="Norway" style="width:100%">
			<div class="w3-container w3-white">
				<h3>EVENT POPULAIRE RAJOUTER FLECHE GAUCHE DROITE</h3>
				<h6 class="w3-opacity">From $99</h6>
				<p>Single bed</p>
				<p>15m<sup>2</sup>COUPER LES DESCRIPTIONS A XXXX CHAR sinon div grossi</p>
				<p class="w3-large"></p>
				<button class="w3-button w3-block w3-black w3-margin-bottom">Choose Room</button>
			</div>
		</div>
		<div class="w3-third w3-margin-bottom">
			<img src="/img/upcoming-event.jpg" alt="Norway" style="width:100%">
			<div class="w3-container w3-white">
				<h3>Double Room</h3>
				<h6 class="w3-opacity">From $149</h6>
				<p>Queen-size bed</p>
				<p>25m<sup>2</sup></p>
				<p class="w3-large"></p>
				<button class="w3-button w3-block w3-black w3-margin-bottom">Choose Room</button>
			</div>
		</div>
		<div class="w3-third w3-margin-bottom">
			<img src="/img/upcoming-event.jpg" alt="Norway" style="width:100%">
			<div class="w3-container w3-white">
				<h3>Deluxe Room</h3>
				<h6 class="w3-opacity">From $199</h6>
				<p>King-size bed</p>
				<p>40m<sup>2</sup></p>
				<p class="w3-large"></p>
				<button class="w3-button w3-block w3-black w3-margin-bottom">Choose Room</button>
			</div>
		</div>
	</div>

	<div class="w3-row-padding w3-padding-16">
		<div class="w3-third w3-margin-bottom">
			<img src="/img/upcoming-event.jpg" alt="Norway" style="width:100%">
			<div class="w3-container w3-white">
				<h3>EVENT SOON RAJOUTER FLECHE GAUCHE DROITE</h3>
				<h6 class="w3-opacity">From $99</h6>
				<p>Single bed</p>
				<p>15m<sup>2</sup> COUPER LES DESCRIPTIONS A XXXX CHAR sinon div grossi</p>
				<p class="w3-large"></p>
				<button class="w3-button w3-block w3-black w3-margin-bottom">Choose Room</button>
			</div>
		</div>
		<div class="w3-third w3-margin-bottom">
			<img src="/img/upcoming-event.jpg" alt="Norway" style="width:100%">
			<div class="w3-container w3-white">
				<h3>Double Room</h3>
				<h6 class="w3-opacity">From $149</h6>
				<p>Queen-size bed</p>
				<p>25m<sup>2</sup></p>
				<p class="w3-large"></p>
				<button class="w3-button w3-block w3-black w3-margin-bottom">Choose Room</button>
			</div>
		</div>
		<div class="w3-third w3-margin-bottom">
			<img src="/img/upcoming-event.jpg" alt="Norway" style="width:100%">
			<div class="w3-container w3-white">
				<h3>Deluxe Room</h3>
				<h6 class="w3-opacity">From $199</h6>
				<p>King-size bed</p>
				<p>40m<sup>2</sup></p>
				<p class="w3-large"></p>
				<button class="w3-button w3-block w3-black w3-margin-bottom">Choose Room</button>
			</div>
		</div>
	</div> -->