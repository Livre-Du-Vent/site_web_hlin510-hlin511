<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
?>

<html>
<head>  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<script src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
	<style>
		.points_interet, .map { width: 100%; }
		.marker { display: none; }
		.popup { display:none; font-size:12pt; color:black; width:150px; height:150px; background-color:rgba(255, 255, 255, 0.79); border-radius: 10px; padding: 15px; border: 2px solid #73AD21; text-align: left;}
	</style>
</head>
<body>
	<center> <h7> Essai OpenLayers avec OSM et JQUERY ! </h7> </center>
	<header class="w3-display-container w3-content" style="max-width:1500px;min-width:800px;min-height:800px">
		
		<div id="accordion" class="points_interet" style="width:100%; height:25%"></div>
		<img id="markerProtoGreen" class="marker" src="/img/markergreen.png" width="50" height="50"/>
		<img id="markerProtoRed" class="marker" src="/img/markerred.png" width="50" height="50"/>
		<div id="popupProto" class="popup"></div>
	</header>
	<script type="text/javascript" src="/js/OL_OSM.js"></script>
</body>
</html>
