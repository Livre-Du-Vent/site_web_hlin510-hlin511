<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lliego International</title>
	<link rel="icon" href="/img/logo1v2.ico" />
	<meta http-equiv="Content-Type" content="text/php; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<link rel="stylesheet" href="/fontawesome-free-5.11.2-web/css/all.css">
	<script src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
</head>
<body class="w3-light-grey">
	<?php error_reporting( E_ALL );?>  
	<?php include 'info_bdd.php';?>
	<?php include 'navbar.php';?>

	<?php
	if(empty($_POST['submit'])){
		echo "1111111";
		if((empty($_POST['titre']))||(empty($_POST['description']))||(empty($_POST['effectif']))||(empty($_POST['date']))||(empty($_POST['heure']))||(empty($_POST['num_rue']))||(empty($_POST['nom_voie']))){
			echo "2222222";
			$msg;
		}
		else{
			
			try{

				$nbIdAdresses;
				$nbIdE;

				$bdd=new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8',$username,$password);
				$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				echo"coucoucouocucouc";

				$rep = $bdd->prepare("SELECT COUNT(*) FROM ADRESSES;");
				$rep->execute();
				$nbIdAdresses = 0 + $rep->fetch()[0] + 1;
				$rep = $bdd->prepare("INSERT INTO ADRESSES VALUES({$nbIdAdresses}, {$_POST['num_rue']}, '{$_POST['nom_voie']}', {$_POST['cp']}, 0, 0);");
				$rep->execute();

				$rep = $bdd->prepare("SELECT COUNT(*) FROM EVENEMENTS;");
				$rep->execute();
				$nbIdE = 0 + $rep->fetch()[0] + 1;
				$rep = $bdd->prepare("INSERT INTO EVENEMENTS VALUES({$nbIdE}, {$nbIdAdresses}, {$_SESSION['id']}, '{$_POST['titre']}', '{$_POST['description']}', {$_POST['effectif']}, '{$_POST['date']}', '{$_POST['heure']}):00' , 0);");
				$rep->execute();
			}
			catch (PDOException $e) {
				$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				$queryAdresse = "DELETE FROM ADRESSES WHERE IDA = {$nbIdAdresses};";
				$rep = $bdd->prepare($queryAdresse);
				$rep->execute();
				//$queryUser = "DELETE FROM EVENEMENTS WHERE IDE = {$nbIdE};";
				//$rep = $bdd->prepare($queryUser);
				//$rep->execute();
				die($msg);
			}
			catch (Exception $e){
				$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				die($msg);
			}
		}

	}

	?>
	<div class='w3-card-4'>

		<div class='w3-container w3-red'>
			<h2>Ajouter un événement</h2>
		</div>

		<form class='w3-container' method ='post'>

			<label>Titre de l'événement</label>
			<input class='w3-input' type='text' autocomplete='on' name='titre'>

			<label>Description de l'événement</label>
			<input class='w3-input' type='text' autocomplete='on' name='description'>

			<label>Nombre de personne maximum</label>
			<input class='w3-input' type='number' autocomplete='on' name='effectif'>

			<label>Date de l'événement</label>
			<input class='w3-input' type='date' autocomplete='on' name='date'>

			<label>Heure de l'événement</label>
			<input class='w3-input' type='time' autocomplete='on' name='heure'>

			<label>Numero de voie</label>
			<input class='w3-input' type='number' autocomplete='on' name='num_rue'>

			<label>Nom de voie</label>
			<input class='w3-input' type='text' autocomplete='on' name='nom_voie'>

			<label>code postal</label>
			<input class='w3-input' type='number' autocomplete='on' name='cp'>

			<button class='w3-btn' type='submit' name='submit'>Ajouter un événement</button>

		</form>

	</div>

	<?php include 'about.php';?>
	<?php include 'footer.php';?>
</body>
</html>