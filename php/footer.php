<!-- Footer -->
<footer class="w3-padding-32 w3-black w3-center w3-margin-top">
	<h5>Retrouvez nous sur</h5>
	<div class="w3-xlarge w3-padding-16">
		 <a target="_blank" href="https://www.facebook.com/lliego.international" class="w3-bar-item w3-button"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
		 <a target="_blank" href="https://www.instagram.com/lliego_international/" class="w3-bar-item w3-button"><i class="fa fa-instagram w3-hover-opacity"></i></a>
		 <a target="_blank" href="https://twitter.com/LliegoI" class="w3-bar-item w3-button"><i class="fa fa-twitter w3-hover-opacity"></i></a>
		 <a href="#" class="w3-bar-item w3-button"><i class="fa fa-linkedin w3-hover-opacity"></i></a>
	</div>
	<p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank" class="w3-hover-text-green">w3.css</a></p>
</footer>