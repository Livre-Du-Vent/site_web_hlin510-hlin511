<?php
$requete = $bdd->prepare("SELECT ide, titre FROM evenements WHERE idcontrib = :var_id");
$requete->execute(array(":var_id" => $_SESSION['id']));

//afficher tous les evenement que le contributeur a ajouté + bouton supprimer evenement
echo "<ul class='w3-ul'>";
while($data_contrib = $requete->fetch()){
	echo "<li>";
	echo $data_contrib['titre'];
	echo "<form method='post'><button class='w3-button'>Supprimer cet événements</button></form>";
	echo "</li>";
}
echo "</ul>";

?>