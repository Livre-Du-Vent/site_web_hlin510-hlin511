<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lliego International</title>
	<link rel="icon" href="/img/logo1v2.ico" />
	<meta http-equiv="Content-Type" content="text/php; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<link rel="stylesheet" href="/fontawesome-free-5.11.2-web/css/all.css">
	<script src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
</head>
<body class="w3-light-grey">
	<?php include 'navbar.php';?>
	<?php 
	if (isset($_POST['signin'])){
		include 'info_bdd.php';
		if(isset($_POST['email']) && isset($_POST['password'])){ // l'invité à cliqué sur le bouton "envoyer"
			try{
				$bdd=new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8',$username,$password);
				//vérification si mail est mot de passe sont correct
				$requete = $bdd->prepare("SELECT * FROM users WHERE mail = :liste_emails ;");
				$requete->execute(array(":liste_emails" => $_POST['email']));
				if($requete){
					$data_user = $requete->fetch();
					if(count($data_user)>1){
						if(strcmp($_POST['password'], $data_user['MDP'])== 0){
							$_SESSION['id'] = $data_user['IDUSER'];
							$_SESSION['nom'] = $data_user['NOM'];
							$_SESSION['prenom'] = $data_user['PRENOM'];
							$_SESSION['email'] = $data_user['MAIL'];
							$_SESSION['type'] = 'user';

															//vérification si l'utilisateur est un contributeur
							$requete = $bdd->prepare("SELECT * from contributeurs WHERE idcontrib = ?;");
							$requete->execute(array($data_user['IDUSER']));
							$data_contrib = $requete->fetch();
							if(count($data_contrib) > 1){
								$_SESSION['type'] = "contrib";
							}

							$requete = $bdd->prepare("SELECT * from administrateurs WHERE idadmin = ?;");
							$requete->execute(array($data_user['IDUSER']));
							$data_admin = $requete->fetch();
							if(count($data_admin) > 1){
								$_SESSION['type'] = "admin";
							}
							header("Location: /index.php"); // Retour page d'accueil
						}
						else{
							 // mot de passe incorrect
							include 'form_signin.php';
							echo "<p> votre mot de passe est incorrect.</p>";
						}
					}
					else{
													 // email incorrect
						include 'form_signin.php';
						echo "<p>votre mail est incorrect<p>";
					}
				}
				else{ 
				// problème de connexion à la BDD
					include 'form_signin.php';
					echo "<p>connexion à base donnée impossible<p>";
				}
			}
			catch (PDOException $e) {
				$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				die($msg);
			}
			catch (Exception $e){
				$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
				die($msg);
			}
		}
	}
	else{
		include 'form_signin.php';
	}
	?>
	<?php include 'about.php';?>
	<?php include 'footer.php';?>
</body>
</html>