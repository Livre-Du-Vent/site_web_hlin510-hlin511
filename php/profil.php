<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lliego International</title>
	<link rel="icon" href="/img/logo1v2.ico" />
	<meta http-equiv="Content-Type" content="text/php; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<link rel="stylesheet" href="/fontawesome-free-5.11.2-web/css/all.css">
	<script src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
</head>
<body class="w3-light-grey">
	<?php error_reporting( E_ALL );?>  
	<?php include 'info_bdd.php';?>
	<?php include 'navbar.php';?>


	<?php

	try{

		$bdd=new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8',$username,$password);


		


		$requete = $bdd->prepare("SELECT COUNT(evenements.ide) AS nb_ev_part FROM evenements, participe WHERE evenements.ide=participe.ide AND date_evenement >= DATE('y-m-d') AND iduser = :var_id ;");
		$requete->execute(array(":var_id" => $_SESSION['id']));
		$data = $requete->fetch();
		echo "<a href='/php/evenement_participe.php' class=' w3-button w3-mobile'  style='width:100%'>Nombre d'événement auquel vous participez: ".$data['nb_ev_part']."</a>";


		$requete = $bdd->prepare("SELECT COUNT(evenements.ide) AS nb_ev_part_old FROM evenements, participe WHERE evenements.ide=participe.ide AND date_evenement < DATE('y-m-d') AND iduser = :var_id ;");
		$requete->execute(array(":var_id" => $_SESSION['id']));
		$data = $requete->fetch();
		echo "<a href='/php/evenement_cree.php' class=' w3-button  w3-mobile'  style='width:100%'>Nombre d'événement auquel vous avez participez: ".$data['nb_ev_part_old']."</a>";


		if((strcmp($_SESSION['type'] , 'admin')==0) || (strcmp($_SESSION['type'] , 'contrib')==0)){
			$requete = $bdd->prepare("SELECT COUNT(ide) AS nb_ev_cree FROM evenements WHERE date_evenement >= DATE('y-m-d') AND idcontrib = :var_id ;");
			$requete->execute(array(":var_id" => $_SESSION['id']));
			$data = $requete->fetch();
			echo "<a href='/php/evenement_cree.php' class=' w3-button  w3-mobile'  style='width:100%'>Nombre d'événement actuel que vous avez cree: ".$data['nb_ev_cree']."</a>";

			$requete = $bdd->prepare("SELECT COUNT(ide) AS nb_ev_cree_old FROM evenements WHERE date_evenement < DATE('y-m-d') AND idcontrib = :var_id ;");
			$requete->execute(array(":var_id" => $_SESSION['id']));
			$data = $requete->fetch();
			echo "<a href='/php/evenement_cree.php' class=' w3-button  w3-mobile'  style='width:100%'>Nombre d'événement passé que vous avez cree: ".$data['nb_ev_cree_old']."</a>";

			echo "<a href='/php/formulaire_creation_evenement.php' class=' w3-button w3-mobile'>Ajouter un événement</a>";
			
		}
	}
	catch (PDOException $e) {
		$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
		die($msg);
	}
	catch (Exception $e){
		$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
		die($msg);
	}

	?>
	
	<?php include 'about.php';?>
	<?php include 'footer.php';?>
</body>
</html>