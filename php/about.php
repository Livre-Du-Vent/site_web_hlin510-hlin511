<div class="w3-row-padding" id="about">
	<div class="w3-col m6">
		<h3>About</h3>
		<h6>Vous êtes enfin arriver au bon endroit pour connecter (partager) tous vos énévements (culturels et sportifs)</h6>
	</div>
	<div class="w3-col m6">
		<!-- Image of location/map -->
		<img src="/img/dpt-info-fds.png" class="w3-image w3-hover-sepia" style="width:100%;">
	</div>
</div>

<div class="w3-row-padding w3-large w3-center" style="margin:32px 0">
	<div class="w3-third"> Place Eugène Bataillon, 34090 Montpellier, France</div>
	<div class="w3-third"> Phone: +33 0467143676</div>
	<div class="w3-third"> Email: lliego_international@mail.com</div>
</div>

<div class="w3-container w3-padding-32 w3-black w3-opacity w3-card w3-hover-opacity-off" style="margin:32px 0;">
	<h2>Inscrivez vous a la newsletter!</h2>
	<p>Vous recevrez un email mensuel des activités en cours et avenir.</p>
	<input class="w3-input w3-border w3-half" type="text" placeholder="Votre adresse email">
	<button type="button" class="w3-button w3-border w3-red w3-half">Abonnez-vous !</button>
</div>

<div class="w3-container" id="contact">
	<h2>Contact</h2>
	<p>Si vous avez des questions, n\'hésitez pas a les posez.</p>
	Montpellier, FR<br>
	Phone: +33 0467143676<br>
	Email: lliego_international@mail.com<br>
	<form action="/action_page.php" target="_blank">
		<p><input class="w3-input w3-half w3-padding-16 w3-border" type="text" placeholder="Nom" required name="nom"></p>
		<p><input class="w3-input w3-half w3-padding-16 w3-border" type="text" placeholder="Email" required name="email"></p>
		<p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Message" required name="messageText"></p>
		<p><button class="w3-button w3-black w3-padding-large" type="submit">Envoyez</button></p>
	</form>
</div>
<!-- End page content -->