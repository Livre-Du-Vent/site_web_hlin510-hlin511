<!-- Navigation Bar -->
<div class="w3-bar w3-white w3-large">
	<a href="/index.php" class="w3-bar-item w3-button w3-red w3-mobile" style="height:43px"><img class="w3-image w3-bar-item w3-button w3-mobile logo" src="/img/logo1v2.png" ></a>
	<a href="#about" class="w3-bar-item w3-button w3-mobile">About</a>
	<a href="/php/contact.php" class="w3-bar-item w3-button w3-mobile">Contact</a>
	<?php
	if (isset($_POST['deconnexion'])){
		session_destroy();
	}
	if(!empty($_SESSION) && isset($_SESSION['nom']) && isset($_SESSION['prenom'])){
		echo "<form method='POST' action='/php/signin.php'><button class='w3-bar-item w3-button w3-mobile w3-right' type='submit' name='deconnexion' target='/php/signin.php'>Deconnexion</button></form>";
		echo "<a href='/php/profil.php' class='w3-bar-item w3-button w3-right w3-light-grey w3-mobile'>".$_SESSION['nom']." ".$_SESSION['prenom']."</a>";
	}
	else{
		echo "<a href='/php/signin.php' class='w3-bar-item w3-button w3-right w3-light-grey w3-mobile'>Sign/Login</a>";
	}
	?>
</div>