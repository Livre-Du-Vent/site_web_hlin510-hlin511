<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lliego International</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="/img/logo1v2.ico" />
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript"  src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script type="text/javascript"  src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script type="text/javascript"  src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
	<script type="text/javascript" src="/js/OL_OSM.js"></script>
	<style>	body,h1,h2,h3,h4,h5,h6{font-family: "Raleway", Arial, Helvetica, sans-serif}</style>
</head>
<body class="w3-light-grey">
	
	<?php include "./navbar.php";?>
	<?php include './bar_search.php'; ?>
	
	<?php
	if(empty($_POST['searchBox'])) {
		$dateDebut = date("Y-m-d");
		$dateFin = date("Y-m-d", strtotime("$dateDebut + 1 day"));
		$nbPersonne = 1;
		$tarif = 0;
		$codePostal = 34090;

		$dbname='bdlliegointernational';
		$host='localhost';
		$strConnection = 'mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8';
		$arrExtraParam = array(1002 => 'SET NAMES utf8');
		$user = 'root';
		$pwd = '';
		try{
			$bdd = new PDO($strConnection, $user, $pwd, $arrExtraParam);
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$queryEvents = "SELECT * FROM evenements, adresses WHERE evenements.ida = adresses.ida";

			if(!empty($_POST['dateDebut'])) {
				$dateDebut = $_POST['dateDebut'];
				$queryEvents = $queryEvents . " AND evenements.DATE_EVENEMENT >= '{$dateDebut}'";
			}
			if(!empty($_POST['dateFin'])) {
				$dateFin = $_POST['dateFin'];
				$queryEvents = $queryEvents . " AND evenements.DATE_EVENEMENT <= '{$dateFin}'";
			}
			if(!empty($_POST['nombrePersonne'])) {
				$nbPersonne = $_POST['nombrePersonne'];
			}
			if(!empty($_POST['tarif'])) {
				$tarif = $_POST['tarif'];
				$queryEvents = $queryEvents . " AND evenements.TARIF <= {$tarif}";
			}
			if(!empty($_POST['codePostal'])) {
				$codePostal = $_POST['codePostal'];
				$queryEvents = $queryEvents . " AND adresses.CP <= {$codePostal}";
			}
			$queryEvents = $queryEvents . ";";

			$rep = $bdd->prepare($queryEvents);
			$rep->execute();

			include 'affichage_evenement.php';
			
		}
		catch (PDOException $e) {
			$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
			die($msg);
		}
		catch (Exception $e){
			$msg = 'Exception dans '.$e->getFile . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
			die($msg);
		}
	}
	?>

	<?php include './about.php';?>
	<?php include './footer.php';?>
</body>
</html>