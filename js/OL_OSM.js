var map = new ol.Map({
	target: 'map',
	layers : [new ol.layer.Tile({source: new ol.source.OSM()})],
	view: new ol.View({
		center: ol.proj.fromLonLat([3.860472807524502,43.62991812023527]),
		zoom: 14 // max 11 pour une vue satellitaire (sat)
	})
});

var marker = document.getElementById('markerProto');
map.addOverlay(new ol.Overlay({
	position: ol.proj.fromLonLat([3.860472807524502,43.62991812023527]),
	element: document.getElementById("markerProto")
}));

var popup = document.getElementById('popupProto');
map.addOverlay(new ol.Overlay({
	offset: [0, -35],
	position: ol.proj.fromLonLat([3.860472807524502,43.62991812023527]),
	element: document.getElementById("popupProto")
}));

function switchMarker(marker) {
	let id = marker.name;
	$("#marker" + id).toggle();
}

function switchPopup(popupId) {
	console.log(popupId)
	$("#popup" + popupId).toggle();
}

// On repertorie dans un objet les listes déjà chargées
// en valeurs : 1 si la liste est affichée, 0 sinon
var listeChargees = {};

$().ready(function() {
	//creation de la liste de plus haut niveau
	$.getJSON('/json/themes.json', function(data) {
		for(objet of data) {
			if (objet.nom != null) {
				$("#accordion").append("<h3 class='" + objet.nom + "'>" + objet.nom + "</h3><div id='" + objet.nom + "'></div>");
				$.getJSON('/json/' + objet.lien, function(data2) {
					for(objet2 of data2) {
						let ar = objet2.nom.slice(0,-1);
						ar = ar.charAt(0).toUpperCase() + ar.slice(1) + "s"
						if(ar.slice(0,2) === "Ho") {
							ar = "Hô" + ar.slice(2);
						}

						$("#" + ar).append("<label><input type='checkbox' name='" + objet2.nom + "'  value='" + objet2.nom + "'onclick='switchMarker(this)'>" + objet2.nom + "</input></label> <br/>");

						let marker;
						if (objet2.icon === "green") {
							marker = $("#markerProtoGreen").clone();
						}
						else if (objet2.icon == "red") {
							marker = $("#markerProtoRed").clone();
						}
						marker.attr('id', "marker" + objet2.nom);
						marker.attr('onmouseover', "switchPopup('" + objet2.nom + "')");
						marker.attr('onmouseout', "switchPopup('" + objet2.nom + "')");
						$("body").append(marker);
						map.addOverlay(new ol.Overlay({
							position: ol.proj.fromLonLat([objet2.long,objet2.lat]),
							element: document.getElementById("marker" + objet2.nom)//markerProto
						}));

						let popup = $("#popupProto").clone();
						popup.attr("id", "popup" + objet2.nom);
						popup.append("<p>"+objet2.description+"</p>");
						$("body").append(popup);
						map.addOverlay(new ol.Overlay({
							offset: [-63,-190],
							position: ol.proj.fromLonLat([objet2.long,objet2.lat]),
							element: document.getElementById("popup" + objet2.nom)
						}));
					}	
				});
			} // END IF
		}
		$("#accordion").accordion({collapsible:true, heightStyle:'content'});
	});
});
