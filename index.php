<?php
		error_reporting(E_ALL);
		empty($_SESSION)? session_start() : print "";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lliego International</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="/img/logo1v2.ico" />
	<link rel="stylesheet" href="/style/w3.css">
	<link rel="stylesheet" href="/style/css.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript"  src="/lib/jquery/jquery-3.4.1.min.js"></script>
	<script type="text/javascript"  src="/lib/jquery-ui-1.12.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="/lib/jquery-ui-1.12.1/jquery-ui.min.css"></link>
	<script type="text/javascript"  src="/lib/open-layers/ol.js"></script>    
	<link rel="stylesheet" href="/lib/open-layers/ol.css"/>
	<style>	body,h1,h2,h3,h4,h5,h6{font-family: "Raleway", Arial, Helvetica, sans-serif}</style>
</head>
<body class="w3-light-grey">
	<?php include 'php/navbar.php';?>
	<?php include 'php/header.php';?>
	<?php include 'php/content.php';?>
	<?php include 'php/about.php';?>
	<?php include 'php/footer.php';?>
	<script type="text/javascript" src="/js/OL_OSM.js"></script>
</body>
</html>